# -*- coding: utf-8 -*-
from django.test import TestCase

__parameters__ = "__parameters__"


def parameterized(*params):
    def decorator(func, params=params):
        setattr(func, __parameters__,
                getattr(func, __parameters__, ()) + params)
        return func
    return decorator


class ParameterizedTestCaseMetaclass(type):
    def __new__(meta, classname, bases, classDict):
        def tuplify(x):
            if not isinstance(x, tuple):
                return (x,)
            return x
        for methodname, method in classDict.items():
            if hasattr(method, __parameters__):
                classDict.pop(methodname)
                parameters = getattr(method, __parameters__)
                delattr(method, __parameters__)
                for parameter in (tuplify(x) for x in parameters):
                    def test_method(self, method=method, parameter=parameter):
                        method(self, *parameter)
                    name = "%s(%s)" % (methodname,
                                       ",".join([str(p) for p in parameter]))
                    classDict[name] = test_method
        return type.__new__(meta, classname, bases, classDict)


class TestParameterizedTestCase(TestCase):
    __metaclass__ = ParameterizedTestCaseMetaclass

    @parameterized("1.0", "2.0")
    @parameterized("3.0")
    def test_single_param(self, api_version):
        self.assertIn(".", api_version)

    @parameterized(("android", "E123"),
                   ("iphone", "E123"))
    def test_double_params(self, device_type, expected_errorcode):
        self.assertEqual("E", expected_errorcode[0])

    def test_testmethod(self):
        self.assertTrue(hasattr(self, "test_single_param(1.0)"))
        self.assertTrue(hasattr(self, "test_single_param(2.0)"))
        self.assertTrue(hasattr(self, "test_single_param(3.0)"))
        self.assertTrue(hasattr(self, "test_double_params(android,E123)"))
        self.assertTrue(hasattr(self, "test_double_params(iphone,E123)"))
